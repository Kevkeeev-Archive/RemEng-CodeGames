function header(){
	console.clear();
	console.log("RemEng - Code Games.");	
	console.log("Source Code: www.github.com/Diamundo-Archive/RemEng-CodeGames/ (Private Repository, sadly)");
	console.log("Copyright: $me, www.github.com/Diamundo");
	console.log("");
}

function about(){
	header();

	console.log("This program is designed to test and execute the solutions for the internal codegames at Remote Engineer.");
	console.log("");
	console.log("For help, yell 'help' or execute HowToUse() in the console.");
	console.log("");
}

function help(){ HowToUse(); }
function h()   { HowToUse(); }
function HowToUse(){
	header();

	console.log("== How to Use == ");
	console.log("To append a new game, create a new <Game>.js file. In this file should be an accompanying '<Game>()' function to call for test-cases, and an 'about<Game>()' function that explains how that particular game works.");

	console.log("");
	aboutDebug();
}
