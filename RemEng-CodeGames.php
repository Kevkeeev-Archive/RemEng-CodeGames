<!DOCTYPE html>
<html lang="en">
	<head>
		<title>RemEng: CodeGames - Diamundo</title>
		<meta charset="utf-8">
		<style>
			Body {
				width: 100%;
				height: 100%;
				margin: 0px;
				text-align: center;
			}
			#header {
				padding: 10px;
				font-size: 20px;
				font-weight: bold;
			}
			.game {
				padding: 10px;
			}
		</style>
	</head>
	<body>
		<div id="header">OPEN YOUR CONSOLE!</div>

		<!-- Load in all .js files -->
		<?php
			$dir = opendir('.'); //the directory of this script.
			while ($file = readdir($dir)) {
				
				if( substr($file, strlen($file)-3, strlen($file)) === '.js' ) { //only *.js files
					echo('<script src="'. $file .'"></script>'); //include the .js file

					if( $file != 'Main.js' && $file != 'Debug.js' ){
						echo(
						'<div class="game">'.
							'<a href="'.
								'javascript:console.clear();'.
								'javascript:about'. substr($file, 0, strlen($file)-3) .'();'. 
//								'javascript:'. substr($file, 0, strlen($file)-3) .'();'.
							'">'. substr($file, 0, strlen($file)-3) .'</a> '.
						'</div>');
					} // also include a clickable link that will execute the about<Game> function
				}
			}
			closedir($dir);
			echo('<script language=javascript> window.onload=about(); </script>'); //execute Main.js:about()
		?>
		<!-- Load in all .js files -->
		
	</body>
</html>
