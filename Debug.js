var debugStatus = false;

function getDebugStatus() { return (debugStatus === false ? false : true); }
function setDebugStatus(newStatus){ debugStatus = (newStatus == false ? false : true); }

function enableDebug() { debugStatus = true; }
function disableDebug(){ debugStatus = false; }
function toggleDebug() { debugStatus = ! getDebugStatus(); }

function Debug ( line ) {
	if(debugStatus){
		console.log("[DBG] " + line);
	}
}

function aboutDebug(){
	console.log("== Debug ==");
	console.log("Simple debug functionality to print a line to the console. Turned off by default.");
	console.log("Supported functions:");
	console.log(" - enableDebug()        > Enables Debug output.");
	console.log(" - disableDebug()       > Disables Debug output.");
	console.log(" - toggleDebug()        > Toggles Debug output.");
	console.log(" - getDebugStatus()     > Returns the current Debug setting (t/f).");
	console.log(" - setDebugStatus(bool) > Sets the Debug setting to the supplied value.");
	console.log(" - Debug(String)        > Writes the given line to the console, if Debugging is turned on.");
}